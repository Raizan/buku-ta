netconvert --remove-edges.by-vclass pedestrian,rail --try-join-tls --osm-files ../map.osm --output-file map.net.xml --remove-edges.isolated --type-files osmNetconvert.typ.xml

python $SUMO_HOME/tools/randomTrips.py -n map.net.xml --seed=42 --fringe-factor 5 -r map.passenger.rou.xml -e 300 --vehicle-class passenger --vclass passenger --prefix veh --min-distance 300 --trip-attributes 'departLane="best"'

python $SUMO_HOME/tools/route2trips.py map.passenger.rou.xml > map.passenger.trips.xml
