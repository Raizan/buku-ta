#ifndef __aomdv_h__
#define __aomdv_h__

//Another include
include <mobilenode.h> //MobileNode

//Another class definition
class AOMDV: public Agent{
	//Another method and attribute definition
	protected:
		int hello_counter;
		float mobility_factor;
		float vehicle_speed;
		MobileNode* iNode;
		float weight_factor;
};
