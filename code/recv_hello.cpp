void
AOMDV::recvHello(Packet *p) {
	// AOMDV code
	struct hdr_ip *ih = HDR_IP(p);
	struct hdr_aomdv_reply *rp = HDR_AOMDV_REPLY(p);
	AOMDV_Neighbor *nb;

	nb = nb_lookup(rp->rp_dst);
	if(nb == 0) {
		nb_insert(rp->rp_dst);
	}
	else {
		nb->nb_expire = CURRENT_TIME +
		(1.5 * ALLOWED_HELLO_LOSS * HELLO_INTERVAL);
	}

	//HM-AOMDV code
	float r = fabs(vehicle_speed - rp->rp_speed);
	int counter = ++hello_counter;
	if(counter == 0) counter = 1;

	//If packet is received the same time when node send HELLO
	float mf_pre = mobility_factor;
	mobility_factor = (pow(1.0 + r, weight_factor) + (mf_pre * (float)(counter - 1))) / (float) counter;
	//---------------------------------------
	// AOMDV code
	// Add a route to this neighbor
	ih->daddr() = index;
	rp->rp_src = ih->saddr();
	rp->rp_first_hop = index;
	recvReply(p);

}
