import collections
import sys

# Usage: python trace_cbr.py <tracefile.tr>
filename = sys.argv[1]

lines = [line.rstrip('\n') for line in open(filename)]
links = dict()
drop_cbk = 0
drop_nrte = 0
drop_arp = 0
for line in lines:
    line = line.split()
    node = line[2]
    packet_id = line[5]
    layer = line[6]

    if layer == "cbr":
        if packet_id in links:
            if node not in links[packet_id]:
                links[packet_id].append(node)
        else:
            links[packet_id] = [node]
        if line[0] == "D":
            drop_cause = "DROP-" + line[4]
            links[packet_id].append(drop_cause)
            if drop_cause == "DROP-CBK":
                drop_cbk += 1
            elif drop_cause == "DROP-NRTE":
                drop_nrte += 1
            elif drop_cause == "DROP-ARP":
                drop_arp += 1

links_sorted = collections.OrderedDict(sorted(links.items()))
for key, value in links_sorted.iteritems():
    print key, value

print "Drop CBK: ", drop_cbk
print "Drop NRTE: ", drop_nrte
print "Drop IFQ ARP: ", drop_arp
print "Total Drop: ", (drop_cbk + drop_nrte + drop_arp)
