set val(chan) Channel/WirelessChannel ;# channel type
set val(prop) Propagation/TwoRayGround ;# radio-propagation model
set val(netif) Phy/WirelessPhy ;# network interface type
set val(mac) Mac/802_11 ;# MAC type
set val(ifq) Queue/DropTail/PriQueue ;# interface queue type
set val(ll) LL ;# link layer type
set val(ant) Antenna/OmniAntenna ;# antenna model
set val(ifqlen) 50 ;# max packet in ifq
set val(rp) AODV ;# routing protocol
set val(nn) 200
set val(cbrsize) 512    ;# 512 Bytes
set val(cbrrate) 2KB
set val(cbrinterval) 1  ;# 1 packet per second
set val(stop) 460.0

set ns_ [new Simulator]
set topo       [new Topography]
$topo load_flatgrid 1075.51 1075.51

$ns_ node-config -adhocRouting $val(rp) -llType $val(ll) -macType $val(mac) -ifqType $val(ifq) -ifqLen $val(ifqlen) -antType $val(ant) -propType $val(prop) -phyType $val(netif) -channel [new $val(chan)] -agentTrace ON -routerTrace ON -macTrace OFF -movementTrace OFF -topoInstance $topo
