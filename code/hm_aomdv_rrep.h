struct hdr_aomdv_reply {
	u_int8_t        rp_type;// Packet Type
	u_int8_t        reserved[2];
	u_int8_t        rp_hop_count;// Hop Count
	nsaddr_t        rp_dst;      // Destination IP Address
	u_int32_t       rp_dst_seqno;// Destination Sequence Number
	nsaddr_t        rp_src;      // Source IP Address
	double	        rp_lifetime; // Lifetime

	double          rp_timestamp;// when corresponding REQ sent;
	
	// used to compute route discovery latency
	// AOMDV code
	u_int32_t       rp_bcast_id; // Broadcast ID of the corresponding RREQ
	nsaddr_t        rp_first_hop;

	//HM-AOMDV code
	float rp_speed;
	float rp_factor;

	inline int size() {
		int sz = 0;
		sz = 6*sizeof(u_int32_t);
		// AOMDV code
		if (rp_type == AOMDVTYPE_RREP) {
			sz += sizeof(u_int32_t);   // rp_bcast_id
			sz += sizeof(nsaddr_t);    // rp_first_hop
		}
		assert (sz >= 0);
		return sz;
	}

};
