BEGIN {
	sendRREQ = 0;
	forwardRREQ = 0;
}

$0 ~/^s.* \(REQUEST\)/ {
	sendRREQ ++;
}

$0 ~/^f.* \(REQUEST\)/ {
	forwardRREQ ++;
}


END {
	printf "Total RREQ: %d\n", (sendRREQ + forwardRREQ);
}
