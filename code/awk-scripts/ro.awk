BEGIN {
        sendLine = 0;
        recvLine = 0;
        errLine  = 0;
}

$0 ~/^s.* \(REQUEST\)/ {
        sendLine ++ ;
}

$0 ~/^s.* \(REPLY\)/ {
        recvLine ++ ;
}

$0 ~/^s.* \(ERROR\)/ {
        errLine ++ ;
}

END {
        printf "Overhead: %d\n", (sendLine + recvLine + errLine);
}
