BEGIN {
        sendLine = 0;
        recvLine = 0;
}

$0 ~/^s.* AGT/ {
        sendLine ++ ;
}

$0 ~/^r.* AGT/ {
        recvLine ++ ;
}

END {
        printf "Sent: %d Recv: %d Ratio: %.4f\n", sendLine, recvLine, (recvLine/sendLine);
}
