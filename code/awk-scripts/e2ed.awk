
BEGIN {
  highest_packet_id = 0;
}
{
  # Using old trace file format
  action = $1;
  time = $2;
  layer = $4;
  packet_id = $6;

  # Check for the latest packet id
  if ( packet_id > highest_packet_id ) {
    highest_packet_id = packet_id;
  }
  # If start_time of packet_id is empty, then get read time
  if ( start_time[packet_id] == 0 ) {
    start_time[packet_id] = time;
  }

  if ( $7 == "cbr" ) {
    # If packet is not dropped
    if ( action != "D" ) {
      # If packet is received
      if ( action == "r" ) {
        # Get received time
        end_time[packet_id] = time;
      }
    }
    # If packet is dropped
    else {
      # There is no "end time"
      end_time[packet_id] = -1;
    }
  }
}
END {
  sigma_duration = 0;
  count = 0;
  for ( packet_id = 0; packet_id <= highest_packet_id; packet_id++ )
  {
    type = packet_type[packet_id];
    start = start_time[packet_id];
    end = end_time[packet_id];
    packet_duration = end - start;
    if ( start < end ) {
        sigma_duration += packet_duration;
        count++;
    }
  }
  if ( count == 0 ) {
    printf("no_packet_counted\n");
  }
  else {
    printf("Average Delay: %.4f\n", sigma_duration / count);
  }
}
