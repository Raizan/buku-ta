void
AOMDV::sendHello() {
	//Allocate packet
	Packet *p = Packet::alloc();
	struct hdr_cmn *ch = HDR_CMN(p);
	struct hdr_ip *ih = HDR_IP(p);
	struct hdr_aomdv_reply *rh = HDR_AOMDV_REPLY(p);

	//HM-AOMDV code
	iNode = (MobileNode*) (Node::get_node_by_address(index));
	vehicle_speed = ((MobileNode *) iNode)->speed();
	hello_counter = 0;

	rh->rp_speed = vehicle_speed;
	mobility_factor = 0;
	//---------------------------

	//Set other parameter in packet and send it
}

