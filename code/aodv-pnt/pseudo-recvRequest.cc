void AODV::recvRequest(Packet *p) {
  ...
  if (rq->rq_dst != index) { // If I'm not destination
    isEligible = false;
    for (u_int32_t i = 0; i < rq->nodes_list_len; ++i) {
      // I'm on the list, so I'm eligible for this packet
      if (rq->rq_eligible_nodes[i] == index) {
        isEligible = true;
        break;
      }
    }
  }
  // If I'm not in relay node set array, then drop packet
  // If I'm in relay node set, then re-compute to create new list
  else if (rq->rq_dst != index) {
    // Calculate TWR and futureTWR
    // Classify TWR
    // Create relay node set
    // Replace rq_eligible_nodes with the re-computed list
  }
  ...
}
