std::sort(listTWR.begin(), listTWR.end(), minTWR()); // ascend
listTWR[0].isOptimal = true; // minimum TWR in the list

for (u_int32_t i = 0; i < listTWR.size(); i++) {
  if (listTWR[i].futureTWR < listTWR[i].TWR) {
    listTWR[i].isFutureBetter = true;
  }

  double deltaTWR = fabs(listTWR[i].TWR - listTWR[i].futureTWR);
  if (deltaTWR < thresholdW) {
    listTWR[i].isStable = true;
  }

  // If Optimal, unstable, better -> Add to Relay list
  // Else if Optimal, stable, not better -> Add to Relay list
  // Else if Suboptimal, unstable, better -> Add to Relay list
  // Else if Suboptimal, stable, not better -> Add to Relay list
}
