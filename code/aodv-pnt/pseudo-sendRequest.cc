void AODV::sendRequest(nsaddr_t dst) {
    // TWR and futureTWR calculation
    .
    .
    // Create relay node set
    // Allocate a RREQ packet
    .
    .
    // Fill out the RREQ packet
    // Fill out common header
    ih->daddr() = IP_BROADCAST; // Broadcast this packet
    // Fill up RREQ fields.

    // AODV-PNT Embed relay node set
    rq->nodes_list_len = eligibleAddrList.size();
    eligibleNodes = new nsaddr_t[rq->nodes_list_len];
    for (u_int32_t i = 0; i < rq->nodes_list_len; i++) {
      eligibleNodes[i] = eligibleAddrList[i];
    }
    rq->rq_eligible_nodes = eligibleNodes;

    Scheduler::instance().schedule(target_, p, 0.);
}
