// TWR Calculation
// Calculate distance between next-hop and dst
double nb_distance;
nb_distance = sqrt(pow((nb->nb_x - xDst), 2) +
                   pow((nb->nb_y - yDst), 2));
// distance between this node and neighbor
double radius = std::min(sqrt(pow((nb->nb_x - posX), 2)
    + pow((nb->nb_y - posY), 2)),(double) maxTxRange);
// link quality between this node and neighbor
double quality = 1.0 / (1.0 - (radius /
          ((double) maxTxRange + 1.0)));
// Value times factor multiplier
double modSpeed     = fSpeed * nb->nb_speed;
double modAccel     = fAccel * nb->nb_accel;
double modDistance  = fDistance * nb_distance;
double modQuality   = fQuality * quality;
// TWR = f s × |S n − S d | + f a × |A n − A d | + f d × dist + f q × Q
double TWR = modSpeed + modAccel + modDistance + modQuality;
