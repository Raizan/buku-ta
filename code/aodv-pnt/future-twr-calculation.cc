// Future speed v' = v + a × t
double nb_speedFuture = nb->nb_speed + (nb->nb_accel *
                        timeModifier);
// Future neighbor position; formula: x' = x + v0 t + 0.5at^2
double nb_xFuture = nb->nb_x + (nb->nb_speed * timeModifier)
    + (0.5 * nb->nb_accel * timeModifier * timeModifier);
double nb_yFuture = nb->nb_y + (nb->nb_speed * timeModifier)
    + (0.5 * nb->nb_accel * timeModifier * timeModifier);
// Future this_node position
double iXFuture = posX + (iSpeed * timeModifier)
    + (0.5 * iAccel * timeModifier * timeModifier);
double iYFuture = posY + (iSpeed * timeModifier)
    + (0.5 * iAccel * timeModifier * timeModifier);
// Calc future distance between next-hop and dst
// Calc distance between this node and neighbor using future values
// Calc link quality
// Calc future TWR
