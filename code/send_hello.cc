void
AOMDV::sendHello() {
	Packet *p = Packet::alloc();
	struct hdr_cmn *ch = HDR_CMN(p);
	struct hdr_ip *ih = HDR_IP(p);
	struct hdr_aomdv_reply *rh = HDR_AOMDV_REPLY(p);

#ifdef DEBUG
	fprintf(stderr, "sending Hello from %d at %.2f\n", index, Scheduler::instance().clock());
#endif // DEBUG

	rh->rp_type = AOMDVTYPE_HELLO;
	//rh->rp_flags = 0x00;
	// AOMDV code
	rh->rp_hop_count = 0;
	rh->rp_dst = index;
	rh->rp_dst_seqno = seqno;
	rh->rp_lifetime = (1 + ALLOWED_HELLO_LOSS) * HELLO_INTERVAL;
	//HM-AOMDV code
	iNode = (MobileNode*) (Node::get_node_by_address(index));
	vehicle_speed = ((MobileNode *) iNode)->speed();
	hello_counter = 0;
	double xpos_t, ypos_t, zpos_t;

	rh->rp_speed = vehicle_speed;
	mobility_factor = 0;
	//-----------------------------

	// ch->uid() = 0;
	ch->ptype() = PT_AOMDV;
	ch->size() = IP_HDR_LEN + rh->size();
	ch->iface() = -2;
	ch->error() = 0;
	ch->addr_type() = NS_AF_NONE;
	ch->prev_hop_ = index;          // AODV hack

	ih->saddr() = index;
	ih->daddr() = IP_BROADCAST;
	ih->sport() = RT_PORT;
	ih->dport() = RT_PORT;
	ih->ttl_ = 1;

	Scheduler::instance().schedule(target_, p, 0.0);
}
