all: compile_book

compile_book: xelatex.run bibtex.run pre_compile.run
	xelatex -shell-escape buku_ta.tex

xelatex.run:
	xelatex -shell-escape buku_ta.tex

bibtex.run:
	bibtex buku_ta

pre_compile.run:
	xelatex -shell-escape buku_ta.tex
